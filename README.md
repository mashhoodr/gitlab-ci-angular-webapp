## Test and Deploy an Angular web application
This example will guide you how to run tests in your Angular web application.

You can checkout the example [source](https://gitlab.com/planet-innovation/gitlab-ci-angular-webapp) and check [CI status](https://gitlab.com/planet-innovation/gitlab-ci-angular-webapp/builds?scope=all).

### Configure project
This is what the `.gitlab-ci.yml` file looks like for this project:
```yaml
image: pijzl/docker-node-karma-protractor-chrome

before_script:
  - npm -v
  - node -v
  - pwd
  - ls -l

stages:
  - dependences
  - gulp

install:
  stage: dependences
  script: 
    - npm install
    - bower install  --allow-root
  artifacts:
    paths:
      - node_modules/
      - bower_components/
    expire_in: 1 week

gulp_test:
  stage: gulp
  script:
    - gulp test

gulp_build:
  stage: gulp
  script:
    - gulp build

gulp_e2e:
  stage: gulp
  script:
    - gulp protractor
```

This project has four jobs:
1. `install` - used to install node_modules and bower_components
2. `gulp_test` - used to run unit test
3. `gulp_e2e` - used to run end to end test
4. `gulp_build` - build production environment

### runner
You can use public runners available on `gitlab.com/ci` or register your own runner.
