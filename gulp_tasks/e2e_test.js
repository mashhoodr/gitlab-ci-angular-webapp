'use strict';

var path = require('path');
var gulp = require('gulp');
const conf = require('../conf/gulp.conf');

var browserSync = require('browser-sync');

var $ = require('gulp-load-plugins')();

gulp.task('webdriver-standalone', $.protractor.webdriver_standalone);
gulp.task('runProtractor', runProtractor);

function runProtractor(done) {
  var params = process.argv;
  var args = params.length > 3 ? [params[3], params[4]] : [];

  gulp.src(path.join(conf.paths.e2e, '/**/*.js'))
    .pipe($.protractor.protractor({
      configFile: 'protractor.conf.js',
      args: args
    }))
    .on('error', function(err) {
      // Make sure failed tests cause gulp to exit non-zero
      throw err;
    })
    .on('end', function() {
      // Close browser sync server
      browserSync.exit();
      done();
      process.exit();
    });
}
